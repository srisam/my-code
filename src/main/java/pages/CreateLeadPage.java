package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods
{

	public CreateLeadPage()
	{
	PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName")
	WebElement cname;
	
	public CreateLeadPage typeCompanyname(String Companyname)
	{
		type(cname,Companyname);
		return this;
	}
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement fname;
	
	public CreateLeadPage typeFirstname(String Firstname)
	{
		type(fname,Firstname);
		return this;
	}
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement lname;
	
	public CreateLeadPage typeLastname(String Lastname)
	{
		type(lname,Lastname);
		return this;
	}
	@FindBy(how=How.CLASS_NAME,using="smallSubmit")
	WebElement sub;
	public void clickSubmit()
	
	{
		click(sub);
		
	}
}











