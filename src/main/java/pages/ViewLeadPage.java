package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods
{

	public ViewLeadPage()
	{
	PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="username")
	WebElement eleUsername;
	@FindBy(how=How.ID,using="password")
	WebElement elePassword;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
	WebElement eleLogin;
	public ViewLeadPage typeUsr(String data)
	{
		type(eleUsername, data);
		return this;
	}
	public ViewLeadPage typePassword(String data)
	{
		type(elePassword, data);
		return this;
	}
	public void clickLogin()
	
	{
		click(eleLogin);
		
	}
}











