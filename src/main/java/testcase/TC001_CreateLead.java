package testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead  extends ProjectMethods
{
@BeforeTest
	public void setData() 
	{
	testCaseName="TC001_CreateLead";
	testCaseDescription="Create A New lead";
	category="Gopi";
	author="smoke";
	dataSheetName="CreateLead";
		
	}
@Test(dataProvider="getdata")
public void createlead( String uname,String password, String cname,String fname,String lname) 

{
	new LoginPage()
	.typeUsr(uname)
	.typePassword(password)
	.clickLogin()
	.clickCrmsfa()
	.clickLeads()
	.clickCreatelead()
	.typeCompanyname(cname)
	.typeFirstname(fname)
	.typeLastname(lname)
	.clickSubmit();
	
	
}

}


