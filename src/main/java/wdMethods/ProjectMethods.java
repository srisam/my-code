package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import util.DataInputProvider;


public class ProjectMethods extends SeMethods
{
public String dataSheetName;
	/*@BeforeSuite(groups="any")
	public void beforeSuite()
	{
		System.out.println("the beforesuite");
	}
	
	@BeforeTest(groups="any")
	
		public void beforeTest()
		{
			System.out.println("the beforeTest");
		}*/
	/*@BeforeClass(groups="any")
	public void beforeClass()
	{
		System.out.println("the before class");
	}*/
	
	//@Parameters({"url","uname","pwd"})
	@BeforeMethod
	public void login() 
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		/*WebElement eleUsername = locateElement("id", "username");
	   type(eleUsername, username);
	   WebElement elePassword = locateElement("id","password");
	   type(elePassword, password);
	   WebElement eleLogin = locateElement("class","decorativeSubmit");
	   click(eleLogin);
	   WebElement eleCrm = locateElement("link", "CRM/SFA");
	   click(eleCrm);
		*/
	}
	/*@AfterMethod
	
	public void afterMethod()
	{
		System.out.println("the aftermethod ");
	}
	
	@AfterClass
	public void afterClass()
	{
		System.out.println("the afterclass");
	}
	@AfterSuite
	public void afterSuite()
	{
		System.out.println("the aftersuite");
	}*/
	@DataProvider(name="getdata")
	public Object[][] fetchdata() throws IOException
	{
		
	Object[][] data = DataInputProvider.readExcel(dataSheetName);
		return data;
	}
	@AfterMethod
	public void close() 
	{
	
		closeBrowser();
	}
	
	
}
